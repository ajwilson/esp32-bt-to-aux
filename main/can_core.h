/* 
	Calculated based on 160 mhz clock, aiming for 95kbps sample point 87.5%
	
	If for any reason the clock rate of the ESP32 is changed, this needs to be recalculated
*/
/* https://www.esacademy.com/en/library/calculators/sja1000-timing-calculator.html */
#define TWAI_TIMING_CONFIG_95KBITS()   {.brp = 52, .tseg_1 = 13, .tseg_2 = 2, .sjw = 2, .triple_sampling = false}

/* Serial Logging IDs */
#define CAN_TAG				  "CAN"

/* CAN IDs */
#define CAN_ID_SWC      	  (0x206u)

#define CAN_DATA_PRESS_STATE  (0u) 
#define CAN_DATA_BUTTON_ID    (1u)
#define CAN_DATA_PRESS_COUNT  (2u)
#define CAN_DATA_MAX          (3u)

/* SWC Button IDs */
#define SWC_PHONE_PICKUP      (0x81u)
#define SWC_PHONE_HANGUP      (0x82u)
#define SWC_SEEK_DIR          (0x83u)
#define SWC_SEEK_PRESS        (0x84u)
#define SWC_BUTTON_UP         (0x91u)
#define SWC_BUTTON_DOWN       (0x92u)
#define SWC_VOL_DIR           (0x93u)

void can_init( void );