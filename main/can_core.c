#include "driver/gpio.h"
#include "driver/twai.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_avrc_api.h"

#include "can_core.h"

#define TWAI_NO_DELAY      (0u)
#define TWAI_TASK_PERIOD   (10u) /* 10ms */

static void can_rx_task_handler( void *pvParameters );
static void can_rx_handle_swc( twai_message_t *msg );

static TaskHandle_t xHandle = NULL;

void can_init( void )
{
    twai_general_config_t g_config = TWAI_GENERAL_CONFIG_DEFAULT(GPIO_NUM_18, GPIO_NUM_19, TWAI_MODE_NORMAL);
    twai_timing_config_t t_config = TWAI_TIMING_CONFIG_95KBITS();
    twai_filter_config_t f_config = TWAI_FILTER_CONFIG_ACCEPT_ALL();

    //Install TWAI driver
    if (twai_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
        ESP_LOGI(CAN_TAG, "Driver installed\n");
    } else {
        ESP_LOGI(CAN_TAG, "Failed to install driver\n");
        return;
    }

    //Start TWAI driver
    if (twai_start() == ESP_OK) {
        ESP_LOGI(CAN_TAG, "Driver started\n");
    } else {
        ESP_LOGI(CAN_TAG, "Failed to start driver\n");
        return;
    }

    /* Create CAN Rx Handler */
    xTaskCreate(can_rx_task_handler, "CanRx", 3072, NULL, configMAX_PRIORITIES - 3, &xHandle);
}

void can_rx_task_handler( void *pvParameters )
{
    twai_message_t rx_msg;

    for( ;; )
    {
        vTaskDelay(TWAI_TASK_PERIOD / portTICK_PERIOD_MS);

        /* Check if we have a message */
        if(twai_receive(&rx_msg, TWAI_NO_DELAY) == ESP_OK)
        {
            ESP_LOGI(CAN_TAG, "CAN ID: %x", rx_msg.identifier );

            switch( rx_msg.identifier )
            {
                case CAN_ID_SWC:
                {
                    /* Handle individual bytes */
                    can_rx_handle_swc( &rx_msg );
                    break;
                }

                default:
                {
                    /* Unknown CAN ID */
                }
            }
        }
    }
}

void can_rx_handle_swc( twai_message_t *msg )
{
    if( msg->data_length_code >= CAN_DATA_MAX )
    {
        switch( msg->data[CAN_DATA_BUTTON_ID] )
        {
            case SWC_PHONE_PICKUP:
            {

                break;
            }

            case SWC_PHONE_HANGUP:
            {

                break;
            }

            case SWC_SEEK_DIR:
            {

                break;
            }

            case SWC_SEEK_PRESS:
            {

                break;
            }

            case SWC_BUTTON_UP:
            {
                esp_avrc_ct_send_passthrough_cmd(0, ESP_AVRC_PT_CMD_FORWARD, ESP_AVRC_PT_CMD_STATE_PRESSED);
                esp_avrc_ct_send_passthrough_cmd(0, ESP_AVRC_PT_CMD_FORWARD, ESP_AVRC_PT_CMD_STATE_RELEASED);
                break;
            }

            case SWC_BUTTON_DOWN:
            {
                esp_avrc_ct_send_passthrough_cmd(0, ESP_AVRC_PT_CMD_BACKWARD, ESP_AVRC_PT_CMD_STATE_PRESSED);
                esp_avrc_ct_send_passthrough_cmd(0, ESP_AVRC_PT_CMD_BACKWARD, ESP_AVRC_PT_CMD_STATE_RELEASED);
                break;
            }

            case SWC_VOL_DIR:
            {

                break;
            }

            default:
            {
                /* Unknown keypress */
            }
        }
    }
}