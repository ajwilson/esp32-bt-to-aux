/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef __BT_APP_NVM_H__
#define __BT_APP_NVM_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define BT_ADDR_BUFF_SIZE 		(6u)

esp_err_t bt_app_nvm_get_addr(uint8_t *addr);
esp_err_t bt_app_nvm_set_addr(uint8_t *addr);


#endif /* __BT_APP_NVM_H__ */
