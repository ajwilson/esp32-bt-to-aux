/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "bt_app_nvm.h"

#define BT_NVM_TAG            "NVM"

esp_err_t bt_app_nvm_get_addr( uint8_t *addr )
{
    nvs_handle_t addr_handle;
    size_t size = BT_ADDR_BUFF_SIZE;
    esp_err_t err = ESP_OK;

    if( addr != NULL )
    {
        err = nvs_open("storage", NVS_READWRITE, &addr_handle);
        if (err != ESP_OK) 
        {
            printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        }
        else
        {
            err = nvs_get_blob(addr_handle, "bt_addr", addr, &size);

            if( err == ESP_OK )
            {
                ESP_LOGI(BT_NVM_TAG, "nvm get bt addr: [%02x:%02x:%02x:%02x:%02x:%02x]", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
            }
            else
            {
                ESP_LOGI(BT_NVM_TAG, "err reading NVM [%s]", esp_err_to_name(err));
            }
        }

        nvs_close(addr_handle);
    }

    return err;
}

esp_err_t bt_app_nvm_set_addr( uint8_t *addr )
{
    nvs_handle_t addr_handle;
    esp_err_t err = ESP_OK;

    if( addr != NULL )
    {
        err = nvs_open("storage", NVS_READWRITE, &addr_handle);
        if (err != ESP_OK) 
        {
            printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        }
        else
        {
            /* Save some EEPROM cycles */

            /* Check existing addr in NVM */
            uint8_t buff[BT_ADDR_BUFF_SIZE] = { 0u }; 
            bt_app_nvm_get_addr( buff );

            bool same = true; 
            for( uint8_t i = 0u; i < BT_ADDR_BUFF_SIZE; i++ )
            {
                if( buff[i] != addr[i] )
                {
                    same = false;
                }
            }

            /* Don't write to NVM if it's already in there */
            if( same == true )
            {
                ESP_LOGI(BT_NVM_TAG, "nvm set bt addr - saved nvm write");
            }
            else
            {
                err = nvs_set_blob(addr_handle, "bt_addr", addr, BT_ADDR_BUFF_SIZE);

                if( err == ESP_OK )
                {
                    nvs_commit(addr_handle);
                    ESP_LOGI(BT_NVM_TAG, "nvm set bt addr [%02x:%02x:%02x:%02x:%02x:%02x]", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
                }
                else
                {
                    ESP_LOGI(BT_NVM_TAG, "err writing nvm [%s]", esp_err_to_name(err));
                }
            }
        }

        nvs_close(addr_handle);
    }

    return err;
}